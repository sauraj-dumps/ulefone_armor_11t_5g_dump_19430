## full_k6873v1_64-user 11 RP1A.200720.011 mp1V8132 release-keys
- Manufacturer: ulefone
- Platform: mt6873
- Codename: Armor_11T_5G
- Brand: Ulefone
- Flavor: full_k6873v1_64-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: 1622016924
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Ulefone/Armor_11T_5G_EEA/Armor_11T_5G:11/RP1A.200720.011/1622016924:user/release-keys
- OTA version: 
- Branch: full_k6873v1_64-user-11-RP1A.200720.011-mp1V8132-release-keys
- Repo: ulefone_armor_11t_5g_dump_19430


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
